import React from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const Navbar = ({ func }) => {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        My App
                    </Typography>
                    <Button sx={{ backgroundColor: "#00c2cb", color: "white", borderRadius: "25px" }} onClick={func}>Add User</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default Navbar;
